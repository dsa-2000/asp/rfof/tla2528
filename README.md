# tla2528

A no-std Rust driver for the [TLA2528](https://www.ti.com/lit/ds/symlink/tla2528.pdf) small, 8-channel I2C adc.

## Unimplemented Features

- High-Speed I2C mode (> 1MHz), the datasheet section on this is unintelligible
- Appending 4-bit channel ID
- Auto sequencing
- General call

## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or
  http://opensource.org/licenses/MIT)

at your option.

### Contributing

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall
be dual licensed as above, without any additional terms or conditions.
