use ftdi_embedded_hal::{self as hal, ftdi};
use tla2528::{Address, Channel, Direction, Drive, Osr, PinMode, Tla2528};

const TEMP: Channel = Channel::Ch0;
const PDI: Channel = Channel::Ch1;
const RF: Channel = Channel::Ch2;
const LNAI: Channel = Channel::Ch3;
const LDI: Channel = Channel::Ch4;
const LNA_FAULT: Channel = Channel::Ch5;
const LNA_EN: Channel = Channel::Ch6;

fn main() {
    let device = ftdi::find_by_vid_pid(0x0403, 0x6010)
        .interface(ftdi::Interface::B)
        .open()
        .unwrap();
    let hal = hal::FtHal::init_freq(device, 7_000).unwrap(); // 1 kHz
    let bus = hal.i2c().unwrap();
    let mut adc = Tla2528::new(bus, Address::default()).reset().unwrap();
    adc.calibrate().unwrap();
    println!("CRC OK?: {}", adc.crc_ok().unwrap());
    // By default, the time between averages is 1us, the highest it can go
    adc.set_oversampling(Osr::_128).unwrap();
    let avg_time = (adc.get_oversample_cycle_time() as f32) * 128.0 / 1e9;
    let implied_clk_speed = 1.0 / avg_time;
    dbg!(implied_clk_speed);

    // Setup pins (should be a noop)
    adc.set_pin_mode(TEMP, PinMode::Analog).unwrap();
    adc.set_pin_mode(PDI, PinMode::Analog).unwrap();
    adc.set_pin_mode(RF, PinMode::Analog).unwrap();
    adc.set_pin_mode(LNAI, PinMode::Analog).unwrap();
    adc.set_pin_mode(LDI, PinMode::Analog).unwrap();
    adc.set_pin_mode(LNA_FAULT, PinMode::Digital(Direction::Input))
        .unwrap();
    adc.set_pin_mode(LNA_EN, PinMode::Digital(Direction::Output(Drive::PushPull)))
        .unwrap();

    println!("Setting LNA on");
    adc.digital_write(LNA_EN, true).unwrap();

    // Read
    let val = adc.analog_read(TEMP).unwrap();
    println!("TEMP: {val}");
    let val = adc.analog_read(PDI).unwrap();
    println!("PDI: {val}");
    let val = adc.analog_read(RF).unwrap();
    println!("RF: {val}");
    let val = adc.analog_read(LNAI).unwrap();
    println!("LNAI: {val}");
    let val = adc.analog_read(LDI).unwrap();
    println!("LDI: {val}");
    println!("nFault: {}", adc.digital_read(LNA_FAULT).unwrap());
}
