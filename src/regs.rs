use packed_struct::prelude::*;

pub(crate) trait Addr {
    const ADDR: u8;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct SystemStatus {
    #[packed_field(bits = "7")]
    _res7: ReservedOne<packed_bits::Bits<1>>,
    #[packed_field(bits = "6")]
    pub(crate) seq_status: bool,
    #[packed_field(bits = "5")]
    pub(crate) i2c_speed: bool,
    #[packed_field(bits = "4")]
    _res4: ReservedZero<packed_bits::Bits<1>>,
    #[packed_field(bits = "3")]
    pub(crate) osr_done: bool,
    #[packed_field(bits = "2")]
    pub(crate) crc_err_fuse: bool,
    #[packed_field(bits = "1")]
    _res1: ReservedZero<packed_bits::Bits<1>>,
    #[packed_field(bits = "0")]
    pub(crate) bor: bool,
}

impl Addr for SystemStatus {
    const ADDR: u8 = 0x0;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct GeneralCfg {
    #[packed_field(bits = "7..=4")]
    _reserved: ReservedZero<packed_bits::Bits<4>>,
    #[packed_field(bits = "3")]
    pub(crate) cnvst: bool,
    #[packed_field(bits = "2")]
    pub(crate) ch_rst: bool,
    #[packed_field(bits = "1")]
    pub(crate) cal: bool,
    #[packed_field(bits = "0")]
    pub(crate) rst: bool,
}

impl Addr for GeneralCfg {
    const ADDR: u8 = 0x1;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct DataCfg {
    #[packed_field(bits = "7")]
    /// Will output fixed code 0xA5A when true
    pub(crate) fix_pat: bool,
    #[packed_field(bits = "6..=0")]
    _reserved: ReservedZero<packed_bits::Bits<7>>,
}

impl Addr for DataCfg {
    const ADDR: u8 = 0x2;
}

#[derive(PrimitiveEnum_u8, Debug, Default, Clone, Copy, PartialEq, Eq)]
/// ADC Oversampling ratio
pub enum Osr {
    #[default]
    _0 = 0,
    _2 = 1,
    _4 = 2,
    _8 = 3,
    _16 = 4,
    _32 = 5,
    _64 = 6,
    _128 = 7,
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct OsrCfg {
    #[packed_field(bits = "7..=3")]
    _reserved: ReservedZero<packed_bits::Bits<7>>,
    #[packed_field(bits = "2..=0", ty = "enum")]
    pub(crate) osr: Osr,
}

impl Addr for OsrCfg {
    const ADDR: u8 = 0x3;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct OpmodeCfg {
    #[packed_field(bits = "7..=5")]
    _reserved: ReservedZero<packed_bits::Bits<3>>,
    #[packed_field(bits = "4")]
    pub(crate) osc_sel: bool,
    #[packed_field(bits = "3..=0")]
    pub(crate) clk_div: u8,
}

impl Addr for OpmodeCfg {
    const ADDR: u8 = 0x4;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
/// Setting these to true configures the pin as GPIO
pub(crate) struct PinCfg {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for PinCfg {
    const ADDR: u8 = 0x5;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
/// Sets the pin direciton for GPIO. `true` is output, `false` is input
pub(crate) struct GpioCfg {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for GpioCfg {
    const ADDR: u8 = 0x7;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
/// Sets the GPIO drive type. `true` is push-pull, `false` is open-drain
pub(crate) struct GpoDriveCfg {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for GpoDriveCfg {
    const ADDR: u8 = 0x9;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
/// Sets the GPIO digital output value
pub(crate) struct GpoValue {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for GpoValue {
    const ADDR: u8 = 0xB;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
/// Reads the GPIO digital input value
pub(crate) struct GpiValue {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for GpiValue {
    const ADDR: u8 = 0xD;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct SequenceCfg {
    #[packed_field(bits = "7..=5")]
    _reserved1: ReservedZero<packed_bits::Bits<3>>,
    #[packed_field(bits = "4")]
    /// True starts auto-sequencing
    pub(crate) seq_start: bool,
    #[packed_field(bits = "3..=2")]
    _reserved2: ReservedZero<packed_bits::Bits<2>>,
    #[packed_field(bits = "1")]
    _reserved3: ReservedZero<packed_bits::Bits<1>>,
    #[packed_field(bits = "0")]
    /// True is auto, false is manual
    pub(crate) seq_mode: bool,
}

impl Addr for SequenceCfg {
    const ADDR: u8 = 0x10;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct ChannelSel {
    #[packed_field(bits = "7..=4")]
    _reserved: ReservedZero<packed_bits::Bits<4>>,
    #[packed_field(bits = "3..=0")]
    /// Only valid for 0-7
    pub(crate) channel: u8,
}

impl Addr for ChannelSel {
    const ADDR: u8 = 0x11;
}

#[derive(PackedStruct, Debug, Default, PartialEq, Eq, Clone, Copy)]
#[packed_struct(bit_numbering = "lsb0", size_bytes = "1")]
pub(crate) struct AutoSeqChSel {
    #[packed_field(bits = "7")]
    pub(crate) _7: bool,
    #[packed_field(bits = "6")]
    pub(crate) _6: bool,
    #[packed_field(bits = "5")]
    pub(crate) _5: bool,
    #[packed_field(bits = "4")]
    pub(crate) _4: bool,
    #[packed_field(bits = "3")]
    pub(crate) _3: bool,
    #[packed_field(bits = "2")]
    pub(crate) _2: bool,
    #[packed_field(bits = "1")]
    pub(crate) _1: bool,
    #[packed_field(bits = "0")]
    pub(crate) _0: bool,
}

impl Addr for AutoSeqChSel {
    const ADDR: u8 = 0x12;
}

#[derive(Debug, Default, Copy, Clone)]
/// Representation of the internal register that we can use as state
#[allow(dead_code)]
pub(crate) struct Registers {
    pub(crate) system_status: SystemStatus,
    pub(crate) general_cfg: GeneralCfg,
    pub(crate) data_cfg: DataCfg,
    pub(crate) osr_cfg: OsrCfg,
    pub(crate) opmode_cfg: OpmodeCfg,
    pub(crate) pin_cfg: PinCfg,
    pub(crate) gpio_cfg: GpioCfg,
    pub(crate) gpo_drive_cfg: GpoDriveCfg,
    pub(crate) gpo_value: GpoValue,
    pub(crate) gpi_value: GpiValue,
    pub(crate) sequence_cfg: SequenceCfg,
    pub(crate) channel_sel: ChannelSel,
    pub(crate) auto_seq_ch_sel: AutoSeqChSel,
}

#[cfg(test)]
mod tests {
    use super::*;
    use paste::paste;

    macro_rules! test_reg_default_addr {
        ($reg:ident, $addr:literal, $val:literal) => {
            paste! {
                #[test]
                fn [<test_reg_ $reg:snake>]() {
                    let packed = $reg::default().pack().unwrap();
                    assert_eq!(packed[0], $val);
                    assert_eq!($reg::ADDR, $addr);
                }
            }
        };
    }

    // From Table 8 in the datasheet
    test_reg_default_addr!(SystemStatus, 0x0, 0x80);
    test_reg_default_addr!(GeneralCfg, 0x1, 0x0);
    test_reg_default_addr!(DataCfg, 0x2, 0x0);
    test_reg_default_addr!(OsrCfg, 0x3, 0x0);
    test_reg_default_addr!(OpmodeCfg, 0x4, 0x0);
    test_reg_default_addr!(PinCfg, 0x5, 0x0);
    test_reg_default_addr!(GpioCfg, 0x7, 0x0);
    test_reg_default_addr!(GpoDriveCfg, 0x9, 0x0);
    test_reg_default_addr!(GpoValue, 0xB, 0x0);
    test_reg_default_addr!(GpiValue, 0xD, 0x0);
    test_reg_default_addr!(SequenceCfg, 0x10, 0x0);
    test_reg_default_addr!(ChannelSel, 0x11, 0x0);
    test_reg_default_addr!(AutoSeqChSel, 0x12, 0x0);
}
