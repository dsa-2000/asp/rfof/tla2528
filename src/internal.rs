use crate::{regs::*, AdcResult, Tla2528};
use embedded_hal::i2c::{I2c, Operation};
use packed_struct::PackedStruct;

#[repr(u8)]
#[allow(dead_code)]
pub(crate) enum Opcode {
    SingleRead = 0b0001_0000,
    SingleWrite = 0b0000_1000,
    SetBit = 0b0001_1000,
    ClearBit = 0b0010_0000,
    BlockRead = 0b0011_0000,
    BlockWrite = 0b0010_1000,
}

/// Internal generic methods
impl<T, E, S> Tla2528<T, S>
where
    T: I2c<Error = E>,
{
    pub(crate) fn read_multi<const N: usize>(&mut self, reg: u8) -> AdcResult<[u8; N], E> {
        let mut buf = [0u8; N];
        self.bus.write_read(
            self.addr as u8,
            &[
                if N == 1 {
                    Opcode::SingleRead
                } else {
                    Opcode::BlockRead
                } as u8,
                reg,
            ],
            &mut buf,
        )?;
        Ok(buf)
    }

    pub(crate) fn write_multi(&mut self, reg: u8, payload: &[u8]) -> AdcResult<(), E> {
        let preamble = [
            if payload.len() == 1 {
                Opcode::SingleWrite
            } else {
                Opcode::BlockWrite
            } as u8,
            reg,
        ];
        let mut operations = [Operation::Write(&preamble), Operation::Write(payload)];
        self.bus.transaction(self.addr as u8, &mut operations)?;
        Ok(())
    }

    pub(crate) fn set_bits(&mut self, reg: u8, bits: u8) -> AdcResult<(), E> {
        self.bus
            .write(self.addr as u8, &[Opcode::SetBit as u8, reg, bits])?;
        Ok(())
    }

    #[allow(dead_code)]
    pub(crate) fn clear_bits(&mut self, reg: u8, bits: u8) -> AdcResult<(), E> {
        self.bus
            .write(self.addr as u8, &[Opcode::ClearBit as u8, reg, bits])?;
        Ok(())
    }
}

/// Generic register methods
impl<T, E, S> Tla2528<T, S>
where
    T: I2c<Error = E>,
{
    pub(crate) fn read_reg<R, const N: usize>(&mut self) -> AdcResult<R, E>
    where
        R: Addr + PackedStruct<ByteArray = [u8; N]>,
    {
        let raw = self.read_multi(R::ADDR)?;
        // We really shouldn't get any unpacking error has all bit values are valid for every register
        let unpacked = R::unpack(&raw).unwrap();
        Ok(unpacked)
    }

    pub(crate) fn write_reg<R, const N: usize>(&mut self, reg: R) -> AdcResult<(), E>
    where
        R: Addr + PackedStruct<ByteArray = [u8; N]>,
    {
        // This shouldn't error
        let bytes = reg.pack().unwrap();
        self.write_multi(R::ADDR, &bytes)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Address;
    use embedded_hal_mock::eh1::i2c::{Mock, Transaction};

    const ADDR: u8 = 0x10;

    fn dev_with_trans(trans: &[Transaction]) -> Tla2528<Mock, crate::typestate::Uninitialized> {
        Tla2528::new(Mock::new(trans), Address::default())
    }

    fn destroy<S>(dev: Tla2528<Mock, S>) {
        dev.destroy().done();
    }

    #[test]
    fn test_create_destroy() {
        let dev = dev_with_trans(&[]);
        destroy(dev);
    }

    #[test]
    fn test_read_multi_single() {
        let reg = 0xDE;
        let payload = [0xAD];
        let trans = [Transaction::write_read(
            ADDR,
            vec![0b0001_0000, reg], // 7.5.1.1 example
            payload.to_vec(),
        )];
        let mut dev = dev_with_trans(&trans);
        let resp: [u8; 1] = dev.read_multi(reg).unwrap();
        assert_eq!(resp, payload);
        destroy(dev);
    }

    #[test]
    fn test_read_multi() {
        let reg = 0xDE;
        let payload = [0xB0, 0xBA];
        let trans = [Transaction::write_read(
            ADDR,
            vec![0b0011_0000, reg], // 7.5.1.2 example
            payload.to_vec(),
        )];
        let mut dev = dev_with_trans(&trans);
        let resp: [u8; 2] = dev.read_multi(reg).unwrap();
        assert_eq!(resp, payload);
        destroy(dev);
    }

    #[test]
    fn test_write_multi_single() {
        let reg = 0xDE;
        let payload = [0xAD];
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(
                ADDR,
                vec![0b0000_1000, reg], // 7.5.2.1 example
            ),
            Transaction::write(ADDR, payload.to_vec()),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = dev_with_trans(&trans);
        dev.write_multi(reg, &payload).unwrap();
        destroy(dev);
    }

    #[test]
    fn test_write_multi() {
        let reg = 0xDE;
        let payload = [0xB0, 0xBA];
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(
                ADDR,
                vec![0b0010_1000, reg], // 7.5.2.4 example
            ),
            Transaction::write(ADDR, payload.to_vec()),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = dev_with_trans(&trans);
        dev.write_multi(reg, &payload).unwrap();
        destroy(dev);
    }

    #[test]
    fn test_set_bits() {
        let reg = 0xDE;
        let bits = 0b11011010;
        let trans = [Transaction::write(ADDR, vec![0b0001_1000, reg, bits])];
        let mut dev = dev_with_trans(&trans);
        dev.set_bits(reg, bits).unwrap();
        destroy(dev);
    }

    #[test]
    fn test_clear_bits() {
        let reg = 0xDE;
        let bits = 0b11011010;
        let trans = [Transaction::write(ADDR, vec![0b0010_0000, reg, bits])];
        let mut dev = dev_with_trans(&trans);
        dev.clear_bits(reg, bits).unwrap();
        destroy(dev);
    }
}
