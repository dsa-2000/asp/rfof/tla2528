#![cfg_attr(not(test), no_std)]

use core::marker::PhantomData;

use crate::regs::*;
use embedded_hal::i2c::I2c;

mod internal;
mod regs;

// Re-export this for the public interface
pub use regs::Osr;

pub(crate) const TIMEOUT_TRIALS: usize = 10;

#[repr(u8)]
#[derive(Debug, Default, Copy, Clone)]
/// TLA2528 I2C addresses
///
/// See the [datasheet](https://www.ti.com/lit/ds/symlink/tla2528.pdf),
/// page 14 for more information
pub enum Address {
    /// R1 = 0 Ω, R2 = DNP
    H17 = 0x17,
    /// R1 = 11 kΩ, R2 = DNP
    H16 = 0x16,
    /// R1 = 33 kΩ, R2 = DNP
    H15 = 0x15,
    /// R1 = 100 kΩ, R2 = DNP
    H14 = 0x14,
    /// R1 = DNP, R2 = DNP
    #[default]
    H10 = 0x10,
    /// R1 = DNP, R2 = 11 kΩ
    H11 = 0x11,
    /// R1 = DNP, R2 = 33 kΩ
    H12 = 0x12,
    /// R1 = DNP, R2 = 100 kΩ
    H13 = 0x13,
}

/// TLA2528 Errors
#[derive(Debug, PartialEq, Eq)]
pub enum Error<E> {
    /// Timeout while polling for an action to complete
    Timeout,
    /// Lower I2C buss error
    I2c(E),
    /// Tried to operate on a pin with an invalid pinmode
    BadPinMode,
    /// Input to the function failed to pass validation
    BadArgument,
}

/// The available pinmodes
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PinMode {
    Analog,
    Digital(Direction),
}

#[allow(non_upper_case_globals)]
impl PinMode {
    /// Alias for digital input
    pub const DigitalInput: Self = Self::Digital(Direction::Input);
    /// Alias for push-pull output
    pub const PushPullOutput: Self = Self::Digital(Direction::Output(Drive::PushPull));
    /// Alias for open drain output
    pub const OpenDrainOutput: Self = Self::Digital(Direction::Output(Drive::OpenDrain));
}

/// The digital pin direction
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Direction {
    Input,
    Output(Drive),
}

/// The digital output drive strength
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Drive {
    OpenDrain,
    PushPull,
}

/// The IO channels
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum Channel {
    Ch0 = 0b0,
    Ch1 = 0b1,
    Ch2 = 0b10,
    Ch3 = 0b11,
    Ch4 = 0b100,
    Ch5 = 0b101,
    Ch6 = 0b110,
    Ch7 = 0b111,
}

impl<E> core::convert::From<E> for Error<E> {
    fn from(e: E) -> Self {
        Error::I2c(e)
    }
}

pub mod typestate {
    /// Typestate marker representing the ADC in an unknown, uninitialized state
    pub struct Uninitialized;
    /// Typestate marker representing the ADC in an initialized, known state
    pub struct Initialized;
}

use typestate::*;

#[derive(Debug)]
pub struct Tla2528<T, S> {
    bus: T,
    regs: Registers,
    addr: Address,
    marker: PhantomData<S>,
}

pub type AdcResult<T, E> = Result<T, Error<E>>;

/// Methods for uninit ADC
impl<T, E> Tla2528<T, Uninitialized>
where
    T: I2c<Error = E>,
{
    /// Construct a new instnace of a [`Tla2528`].
    pub fn new(bus: T, addr: Address) -> Self {
        Self {
            bus,
            regs: Default::default(),
            addr,
            marker: PhantomData,
        }
    }

    /// Reset the device, bringing it to a known state
    pub fn reset(mut self) -> AdcResult<Tla2528<T, Initialized>, E> {
        // Set the reset bit
        self.set_bits(GeneralCfg::ADDR, 1 << 0)?;
        // Poll the reset bit for completion
        for _ in 0..TIMEOUT_TRIALS {
            let reg: GeneralCfg = self.read_reg()?;
            if !reg.rst {
                let init = Tla2528 {
                    regs: Default::default(),
                    bus: self.bus,
                    addr: self.addr,
                    marker: PhantomData,
                };
                return Ok(init);
            }
        }
        Err(Error::Timeout)
    }
}

/// Generic public methods that don't rely on internal state
impl<T, E, S> Tla2528<T, S>
where
    T: I2c<Error = E>,
{
    /// Destroy the instance and return the bus
    pub fn destroy(self) -> T {
        self.bus
    }

    /// Calibrate the ADC offset
    pub fn calibrate(&mut self) -> AdcResult<(), E> {
        // Set the cal bit
        self.set_bits(GeneralCfg::ADDR, 1 << 1)?;
        // Poll the cal bit for completion
        for _ in 0..TIMEOUT_TRIALS {
            let reg: GeneralCfg = self.read_reg()?;
            if !reg.cal {
                return Ok(());
            }
        }
        Err(Error::Timeout)
    }

    /// Checks the CRC fuse status, returning `true` on Ok.
    ///
    /// To re-evaluate this, power cycle or reset the device.
    pub fn crc_ok(&mut self) -> AdcResult<bool, E> {
        let reg: SystemStatus = self.read_reg()?;
        Ok(!reg.crc_err_fuse)
    }

    /// Check the brown-out status bit, retruning `true` on brown-out detection or device power cycle
    /// Reset to clear with [`bor_reset`]
    pub fn browned_out(&mut self) -> AdcResult<bool, E> {
        let reg: SystemStatus = self.read_reg()?;
        Ok(reg.bor)
    }

    /// Resets the brown out detection bit
    pub fn bor_reset(&mut self) -> AdcResult<(), E> {
        self.set_bits(SystemStatus::ADDR, 1 << 0)?;
        Ok(())
    }
}

macro_rules! chan_to_field {
    ($chan:ident, $obj:expr) => {
        match $chan {
            Channel::Ch0 => $obj._0,
            Channel::Ch1 => $obj._1,
            Channel::Ch2 => $obj._2,
            Channel::Ch3 => $obj._3,
            Channel::Ch4 => $obj._4,
            Channel::Ch5 => $obj._5,
            Channel::Ch6 => $obj._6,
            Channel::Ch7 => $obj._7,
        }
    };
}

macro_rules! set_chan_field {
    ($chan:ident, $obj:expr, $val: expr) => {
        match $chan {
            Channel::Ch0 => $obj._0 = $val,
            Channel::Ch1 => $obj._1 = $val,
            Channel::Ch2 => $obj._2 = $val,
            Channel::Ch3 => $obj._3 = $val,
            Channel::Ch4 => $obj._4 = $val,
            Channel::Ch5 => $obj._5 = $val,
            Channel::Ch6 => $obj._6 = $val,
            Channel::Ch7 => $obj._7 = $val,
        }
    };
}

impl<T, E> Tla2528<T, Initialized>
where
    T: I2c<Error = E>,
{
    /// Set the ADC core output to always read `0xA5A`
    pub fn set_fixed_pattern(&mut self, enabled: bool) -> AdcResult<(), E> {
        // Check if the bits match and only perform the write if they're different
        if self.regs.data_cfg.fix_pat ^ enabled {
            // They don't match, update state
            self.regs.data_cfg.fix_pat = enabled;
            // Write state, no need to set_bit as it's the same amount of data
            self.write_reg(self.regs.data_cfg)?;
        }
        // Else nothing to do!
        Ok(())
    }

    /// Gets the current status of fixed output. Returns `true` is the fixed pattern output is enabled
    pub fn get_fixed_pattern(&self) -> bool {
        // We're only introspecting internal state
        self.regs.data_cfg.fix_pat
    }

    /// Get the current pinmode of a channel
    pub fn get_pin_mode(&self, chan: Channel) -> PinMode {
        let pin = chan_to_field!(chan, self.regs.pin_cfg);
        let gpio = chan_to_field!(chan, self.regs.gpio_cfg);
        let drive = chan_to_field!(chan, self.regs.gpo_drive_cfg);
        // From Table 3 in the datasheet
        match [pin, gpio, drive] {
            [false, _, _] => PinMode::Analog,
            [true, false, _] => PinMode::Digital(Direction::Input),
            [true, true, false] => PinMode::Digital(Direction::Output(Drive::OpenDrain)),
            [true, true, true] => PinMode::Digital(Direction::Output(Drive::PushPull)),
        }
    }

    /// Set the pinmode of a channel
    pub fn set_pin_mode(&mut self, chan: Channel, mode: PinMode) -> AdcResult<(), E> {
        if mode == self.get_pin_mode(chan) {
            // Nothing to do!
            return Ok(());
        }
        match mode {
            PinMode::Analog => {
                set_chan_field!(chan, self.regs.pin_cfg, false);
                self.write_reg(self.regs.pin_cfg)?;
            }
            PinMode::Digital(Direction::Input) => {
                set_chan_field!(chan, self.regs.pin_cfg, true);
                self.write_reg(self.regs.pin_cfg)?;
                set_chan_field!(chan, self.regs.gpio_cfg, false);
                self.write_reg(self.regs.gpio_cfg)?;
            }
            PinMode::Digital(Direction::Output(Drive::OpenDrain)) => {
                set_chan_field!(chan, self.regs.pin_cfg, true);
                self.write_reg(self.regs.pin_cfg)?;
                set_chan_field!(chan, self.regs.gpio_cfg, true);
                self.write_reg(self.regs.gpio_cfg)?;
                set_chan_field!(chan, self.regs.gpo_drive_cfg, false);
                self.write_reg(self.regs.gpo_drive_cfg)?;
            }
            PinMode::Digital(Direction::Output(Drive::PushPull)) => {
                set_chan_field!(chan, self.regs.pin_cfg, true);
                self.write_reg(self.regs.pin_cfg)?;
                set_chan_field!(chan, self.regs.gpio_cfg, true);
                self.write_reg(self.regs.gpio_cfg)?;
                set_chan_field!(chan, self.regs.gpo_drive_cfg, true);
                self.write_reg(self.regs.gpo_drive_cfg)?;
            }
        }
        Ok(())
    }

    /// Perform a single-shot ADC conversion on the selected channel.
    ///
    /// ## Notes
    /// This operation will clock stretch. On devices with poor clock stretching
    /// support (many of them), you'll need to run the i2c bus at a lower
    /// speed to compensate. Alternativley, you can just not oversample and
    /// instead do the averaging on the controller.
    ///
    /// ## Format
    /// If we're oversampling, the returned counts will be 16 bits, otherwise
    /// it will be 12 bits, which is the native bit depth of the core.
    pub fn analog_read(&mut self, chan: Channel) -> AdcResult<u16, E> {
        // First check the pinmode
        if self.get_pin_mode(chan) != PinMode::Analog {
            return Err(Error::BadPinMode);
        }
        // Now we follow the flowchart in "manual mode" (Datasheet 7.4.2)
        // SEQ_MODE should be 0 on startup and we don't set it and we've checked the PIN_CFG above
        // Set the channel in manual_ch (if we need to)
        if self.regs.channel_sel.channel != chan as u8 {
            self.regs.channel_sel.channel = chan as u8;
            self.write_reg(self.regs.channel_sel)?;
        }
        // Then read two bytes as we're not going to support channel ID appending, because we're not going to support auto-sequence
        let mut bytes = [0u8; 2];
        self.bus.read(self.addr as u8, &mut bytes)?;
        // Then reconstruct the u16
        let mut res = u16::from_be_bytes(bytes);
        if self.regs.osr_cfg.osr == Osr::_0 {
            // No oversampling
            res >>= 4;
        }
        Ok(res)
    }

    /// Read the digital state of a GPIO input pin (if configured as such).
    ///
    /// Will return `true` for logical high.
    pub fn digital_read(&mut self, chan: Channel) -> AdcResult<bool, E> {
        // First check the pinmode
        if self.get_pin_mode(chan) != PinMode::Digital(Direction::Input) {
            return Err(Error::BadPinMode);
        }
        // Now we just read the gpi register and extract that pin
        let reg: GpiValue = self.read_reg()?;
        Ok(chan_to_field!(chan, reg))
    }

    /// Read the state of multiple GPIO input pins in one transaction.
    ///
    /// Will return `true` for logical high.
    pub fn bulk_digital_read<const N: usize>(
        &mut self,
        chans: &[Channel; N],
    ) -> AdcResult<[bool; N], E> {
        // Check pinmodes
        for chan in chans {
            if self.get_pin_mode(*chan) != PinMode::Digital(Direction::Input) {
                return Err(Error::BadPinMode);
            }
        }
        // Read the whole gpi register
        let reg: GpiValue = self.read_reg()?;
        // Extract the pins in order
        let mut states = [false; N];
        for (i, chan) in chans.iter().enumerate() {
            states[i] = chan_to_field!(chan, reg);
        }
        Ok(states)
    }

    /// Write the digital state of a GPIO output pin (if configured as such).
    ///
    /// `true` is logical high.
    pub fn digital_write(&mut self, chan: Channel, val: bool) -> AdcResult<(), E> {
        // Check pinmode
        if !matches!(
            self.get_pin_mode(chan),
            PinMode::Digital(Direction::Output(_))
        ) {
            return Err(Error::BadPinMode);
        }
        // Set the GPO (alway do the transaction because it's unclear from
        // the datashet if switching between pinmodes resets this register)
        let mut reg = GpoValue::default();
        set_chan_field!(chan, reg, val);
        self.write_reg(reg)?;
        Ok(())
    }

    /// Set the timing control for oversampling.
    ///
    /// `clk_div` is only valid 0-15
    ///
    /// Note: This does not change the conversion time, only
    /// the timing of subsequent samples in oversampling.
    pub fn set_oversample_timing(&mut self, high_speed: bool, clk_div: u8) -> AdcResult<(), E> {
        if clk_div > 0b1111 {
            return Err(Error::BadArgument);
        }
        // Update state and write
        if self.regs.opmode_cfg.osc_sel != !high_speed || self.regs.opmode_cfg.clk_div != clk_div {
            self.regs.opmode_cfg.osc_sel = !high_speed;
            self.regs.opmode_cfg.clk_div = clk_div;
            self.write_reg(self.regs.opmode_cfg)?;
        }
        Ok(())
    }

    /// Get the current oversampling timing settings
    pub fn get_oversample_timing(&self) -> (bool, u8) {
        (!self.regs.opmode_cfg.osc_sel, self.regs.opmode_cfg.clk_div)
    }

    /// Get the current oversampling cycle time in ns.
    /// Multiply this by the oversampling ratio to get the total read time.
    pub fn get_oversample_cycle_time(&self) -> u32 {
        // Is there math for this?
        let (high_speed, clk_div) = self.get_oversample_timing();
        match (high_speed, clk_div) {
            // OSC_SEL = 0
            (true, 0) => 1000,
            (true, 1) => 1500,
            (true, 2) => 2000,
            (true, 3) => 3000,
            (true, 4) => 4000,
            (true, 5) => 6000,
            (true, 6) => 8000,
            (true, 7) => 12000,
            (true, 8) => 16000,
            (true, 9) => 24000,
            (true, 10) => 32000,
            (true, 11) => 48000,
            (true, 12) => 64000,
            (true, 13) => 96000,
            (true, 14) => 128000,
            (true, 15) => 192000,
            // OSC_SEL = 1
            (false, 0) => 32000,
            (false, 1) => 48000,
            (false, 2) => 64000,
            (false, 3) => 96000,
            (false, 4) => 128000,
            (false, 5) => 192000,
            (false, 6) => 256000,
            (false, 7) => 384000,
            (false, 8) => 512000,
            (false, 9) => 768000,
            (false, 10) => 1024000,
            (false, 11) => 15386000,
            (false, 12) => 2048000,
            (false, 13) => 3072000,
            (false, 14) => 4096000,
            (false, 15) => 6144000,
            _ => unreachable!(),
        }
    }

    /// Set the oversampling ratio for the ADC conversion result.
    pub fn set_oversampling(&mut self, osr: Osr) -> AdcResult<(), E> {
        if self.regs.osr_cfg.osr != osr {
            self.regs.osr_cfg.osr = osr;
            self.write_reg(self.regs.osr_cfg)?;
        }
        Ok(())
    }

    /// Get the current oversampling ratio
    pub fn get_oversampling(&self) -> Osr {
        self.regs.osr_cfg.osr
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::internal::Opcode;
    use embedded_hal_mock::eh1::i2c::{Mock, Transaction};

    const ADDR: u8 = 0x10;

    fn destroy<S>(dev: Tla2528<Mock, S>) {
        dev.destroy().done();
    }

    fn new_with_rst(trans: &[Transaction]) -> Tla2528<Mock, crate::typestate::Initialized> {
        let mut t = vec![
            Transaction::write(
                ADDR,
                vec![Opcode::SetBit as u8, GeneralCfg::ADDR, 1 << 0], // Reset
            ),
            Transaction::write_read(
                ADDR,
                vec![Opcode::SingleRead as u8, GeneralCfg::ADDR], // Read GeneralCfg
                vec![0x0],                                        // Cleared rst bit
            ),
        ];
        t.extend_from_slice(trans);
        let bus = Mock::new(&t);
        Tla2528::new(bus, Address::default()).reset().unwrap()
    }

    #[test]
    fn test_new_reset() {
        let dev = new_with_rst(&[]);
        destroy(dev);
    }

    // #[test]
    // fn test_failed_reset() {
    //     let mut trans = vec![Transaction::write(
    //         ADDR,
    //         vec![Opcode::SetBit as u8, GeneralCfg::ADDR, 1 << 0], // Reset
    //     )];
    //     for _ in 0..(TIMEOUT_TRIALS) {
    //         trans.push(Transaction::write_read(
    //             ADDR,
    //             vec![Opcode::SingleRead as u8, GeneralCfg::ADDR], // Read GeneralCfg
    //             vec![1],                                          // Bad rst bit
    //         ))
    //     }
    //     let bus = Mock::new(&trans);
    //     let dev = Tla2528::new(bus, Address::default());
    //     match dev {
    //         Ok(_) => panic!("This should have failed"),
    //         Err((e, mut bus)) => {
    //             assert!(matches!(e, Error::Timeout));
    //             bus.done();
    //         }
    //     }
    // }

    #[test]
    fn test_crc() {
        let trans = [Transaction::write_read(
            ADDR,
            vec![Opcode::SingleRead as u8, SystemStatus::ADDR], // Read SystemStatus
            vec![0b10000000],                                   // Default
        )];
        let mut dev = new_with_rst(&trans);
        assert!(dev.crc_ok().unwrap());
        destroy(dev);
    }

    #[test]
    fn test_bor() {
        let trans = [Transaction::write_read(
            ADDR,
            vec![Opcode::SingleRead as u8, SystemStatus::ADDR], // Read SystemStatus
            vec![0b10000001],                                   // Set bor bit
        )];
        let mut dev = new_with_rst(&trans);
        assert!(dev.browned_out().unwrap());
        destroy(dev);
    }

    #[test]
    fn test_reset_bor() {
        let trans = [Transaction::write(
            ADDR,
            vec![Opcode::SetBit as u8, SystemStatus::ADDR, 0b00000001], // Set SystemStatus bor
        )];
        let mut dev = new_with_rst(&trans);
        dev.bor_reset().unwrap();
        destroy(dev);
    }

    #[test]
    fn test_fixed_pat() {
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, DataCfg::ADDR]),
            Transaction::write(ADDR, vec![0b10000000]),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = new_with_rst(&trans);
        // Setting this twice should only write once
        dev.set_fixed_pattern(true).unwrap();
        dev.set_fixed_pattern(true).unwrap();
        // And reading won't perform a read
        assert!(dev.get_fixed_pattern());
        destroy(dev);
    }

    #[test]
    fn test_set_pinmode_analog() {
        // Should do nothing as that's the default state
        let mut dev = new_with_rst(&[]);
        let mode = PinMode::Analog;
        dev.set_pin_mode(Channel::Ch0, mode).unwrap();
        assert_eq!(dev.get_pin_mode(Channel::Ch0), mode);
        destroy(dev)
    }

    #[test]
    fn test_pinmode_digital_input() {
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, PinCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, GpioCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000000]),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = new_with_rst(&trans);
        let mode = PinMode::Digital(Direction::Input);
        dev.set_pin_mode(Channel::Ch0, mode).unwrap();
        assert_eq!(dev.get_pin_mode(Channel::Ch0), mode);
        destroy(dev)
    }

    #[test]
    fn test_pinmode_digital_output_open_drain() {
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, PinCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, GpioCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, GpoDriveCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000000]),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = new_with_rst(&trans);
        let mode = PinMode::Digital(Direction::Output(Drive::OpenDrain));
        dev.set_pin_mode(Channel::Ch0, mode).unwrap();
        assert_eq!(dev.get_pin_mode(Channel::Ch0), mode);
        destroy(dev)
    }

    #[test]
    fn test_pinmode_digital_output_push_pull() {
        let trans = [
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, PinCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, GpioCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
            Transaction::transaction_start(ADDR),
            Transaction::write(ADDR, vec![Opcode::SingleWrite as u8, GpoDriveCfg::ADDR]),
            Transaction::write(ADDR, vec![0b00000001]),
            Transaction::transaction_end(ADDR),
        ];
        let mut dev = new_with_rst(&trans);
        let mode = PinMode::Digital(Direction::Output(Drive::PushPull));
        dev.set_pin_mode(Channel::Ch0, mode).unwrap();
        assert_eq!(dev.get_pin_mode(Channel::Ch0), mode);
        destroy(dev)
    }
}
